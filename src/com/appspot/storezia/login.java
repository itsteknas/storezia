package com.appspot.storezia;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;

@SuppressWarnings("serial")
public class login extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		
		String email = req.getParameter("email");
		String password = req.getParameter("password");
		
		Query q = new Query("user").addFilter("email", Query.FilterOperator.EQUAL, email);
		PreparedQuery pq = ds.prepare(q);
		
		
		
		
		for (Entity user : pq.asIterable()) {
			
			
			String passwd = (String) user.getProperty("password");
			
			if(passwd.equals(password)){
				resp.getWriter().print(user.getKey().getId());
				Cookie userkey = new Cookie("userkey",String.valueOf(user.getKey().getId()));
				userkey.setSecure(false);
				userkey.setValue(String.valueOf(user.getKey().getId()));
				userkey.setMaxAge(60*60*24);
				userkey.setPath("/");
				resp.addCookie(userkey);
				break;
			}
			else {
				resp.getWriter().print("false");
			}
		}
		// run only the first time
		/*
		Entity item = new Entity("item");
		item.setProperty("category", "technology");
		item.setProperty("description", "a great cell, must buy ! It's gonna beat the crap out of Samsung !");
		item.setProperty("image", "/webface/static/images/xiaomi.jpg");
		item.setProperty("name", "Xiaomi mi3");
		item.setProperty("price", 14000);
		item.setProperty("quantity-available", 50000);
		
		ds.put(item);
		*/
	}

}
