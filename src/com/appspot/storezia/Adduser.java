package com.appspot.storezia;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@SuppressWarnings("serial")
public class Adduser extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		//add exception for username exists
		
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		String emailid = req.getParameter("email");
		
		Entity user = new Entity("user");
		user.setProperty("username", username);
		user.setProperty("password", password);
		user.setProperty("email", emailid);
		
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		ds.put(user);
		
		
		//email the user 
		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);

		String msgBody = 	"Thank you for registering with storezia. \n" +
							"Your email username is "+username+"\n"+
							"Your password is "+password+"\n"+
							"Please login at http://storezia-project.appspot.com/webface/login.html";

		try {
		    Message msg = new MimeMessage(session);
		    msg.setFrom(new InternetAddress("sanket.berde@gmail.com", "Storezia Admin"));
		    msg.addRecipient(Message.RecipientType.TO,
		     new InternetAddress(emailid, username));
		    msg.setSubject("Welcome to Storezia");
		    msg.setText(msgBody);
		    Transport.send(msg);

		} catch (AddressException e) {
		    // ...
		} catch (MessagingException e) {
		    // ...
		}
		
		resp.getWriter().println("User Added Successfully");
		
	}
}
