package com.appspot.storezia;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.KeyFactory;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@SuppressWarnings("serial")
public class Order extends HttpServlet{
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		
		boolean isUserLoggedIn = false ;
		
		long itemkey = Long.valueOf(req.getParameter("itemkey"));
		int quantity = Integer.valueOf(req.getParameter("quantity"));
		long userkey = 0;
		
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		
		
		Cookie[] cookies = req.getCookies();
		try{
		if (cookies.length>0){
			for(Cookie c: cookies ){
				if(c.getName().equals("userkey")){
					isUserLoggedIn = true;
					userkey = Long.valueOf(c.getValue());
				}
			}
		}
		}catch(Exception e){}
		if(isUserLoggedIn) {
			try {
				
					Entity user = ds.get(KeyFactory.createKey("user", userkey));
					Entity item =ds.get(KeyFactory.createKey("item", itemkey));
					
					int quantityAvailable = Integer.valueOf(String.valueOf(item.getProperty("quantity-available")));
					
					if (quantityAvailable<=0) throw new Exception("Item-Sold-Out");
					
					//reduce the quantity avalible in store
					item.setProperty("quantity-available", quantityAvailable - quantity);
					
					
					String username = (String) user.getProperty("username");
					int price = Integer.valueOf(String.valueOf(item.getProperty("price")));
					int cost = quantity * price ;
					
					Entity order = new Entity ("order");
					order.setProperty("userkey", user.getKey().getId());
					order.setProperty("username",username);
					order.setProperty("itemkey", item.getKey().getId());
					order.setProperty("quantity", quantity);
					order.setProperty("price",price);
					order.setProperty("total-cost", cost);
					order.setProperty("timestamp", Calendar.getInstance().getTime());
				
					ds.put(item);
					ds.put(order);
					
					//email order receipt
					Properties props = new Properties();
					Session session = Session.getDefaultInstance(props, null);

					String msgBody = 	"Order Receipt \n" +
										"Order for "+item.getProperty("name").toString() +" placed successfully.\n" +
										"Quantity:"+quantity+" Units \n" +
										"Total price payable:"+cost+"/- Rs. \n"+
										"Thank you for shopping with Storezia.";

					try {
					    Message msg = new MimeMessage(session);
					    msg.setFrom(new InternetAddress("sanket.berde@gmail.com", "Storezia Admin"));
					    msg.addRecipient(Message.RecipientType.TO,
					     new InternetAddress(user.getProperty("email").toString(), username));
					    msg.setSubject("Order Receipt");
					    msg.setText(msgBody);
					    Transport.send(msg);

					} catch (AddressException e) {
					    // ...
					} catch (MessagingException e) {
					    // ...
					}
					
					resp.getWriter().println("Order Successfully Placed.\nCheck email for receipt");

			} catch (EntityNotFoundException e) {
				e.printStackTrace();
				resp.getWriter().print("invalid parameters supplied");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				String error = e.getMessage();
				
				if (error.equals("Item-Sold-Out")){
					resp.getWriter().println("Sorry, this item is Sold out");
				}
				
			}
		}
		else{
			resp.getWriter().print("Please login first.");
		}
		
		
		
	}

}
