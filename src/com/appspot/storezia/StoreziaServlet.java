package com.appspot.storezia;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;

@SuppressWarnings("serial")
public class StoreziaServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		
		
		Entity user = new Entity("user");
		user.setProperty("name", "sanket");
		user.setProperty("passwd", "yolo");

		
		ds.put(user);
		
		
		
			
		resp.setContentType("text/plain");
		resp.getWriter().println("Hello, world");
	}
}
