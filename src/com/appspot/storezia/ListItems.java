package com.appspot.storezia;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.gson.Gson;

@SuppressWarnings("serial")
public class ListItems extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		
		Query q = new Query("item");
		PreparedQuery pq = ds.prepare(q);
		Gson gson = new Gson();
		
		resp.setContentType("text/plain");
		//print everything in the item class !
		resp.getWriter().print("[");
		
		int len = pq.countEntities();
		int i=1;
		
		for (Entity result : pq.asIterable()) {
			
			  resp.getWriter().print(gson.toJson(result));
			  
			  i++;
			  if (i<=len) resp.getWriter().print(",");
			}
		resp.getWriter().print("]");
		
	}

	
}
